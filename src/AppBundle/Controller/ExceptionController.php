<?php
/**
 * Created by PhpStorm.
 * User: siklol
 * Date: 20.11.15
 * Time: 22:32
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Debug\Exception\FlattenException;

class ExceptionController extends Controller
{
    /**
     * @param FlattenException $exception
     */
    public function showAction(FlattenException $exception)
    {
        return [
            'error' => $exception->getMessage(),
            'code' => $exception->getCode(),
            'class' => $exception->getClass(),
            'statusCode' => $exception->getStatusCode()
        ];
    }
}