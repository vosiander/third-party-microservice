<?php
namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class DefaultController extends FOSRestController
{
    /**
     * Returns the current version of the microservice
     *
     * @ApiDoc()
     *
     * @return array
     */
    public function getVersionAction()
    {
        return [
            'version' => 0.1
        ];
    }
}