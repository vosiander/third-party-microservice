<?php

namespace AppBundle;

use AppBundle\DependencyInjection\DomainEventCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle
{
}
