# Metropolis third party Microservice

When you find any errors please report, or better correct them directly. Thank you :)

## Prerequisites

Install [Vagrant](https://www.vagrantup.com/). 

### Install Vagrant based on VirtualBox

Install nfs

    sudo apt-get install nfs-kernel-server

Install [VirtualBox](https://www.virtualbox.org/).

Install vagrant plugins

    vagrant plugin install vagrant-vbguest vagrant-hostmanager vagrant-cachier vagrant-proxyconf
    
Start vagrant on VirtualBox. Switch into the project directory and execute

    vagrant up --provider=virtualbox
    
## Setup

### Start Vagrant

Boot the VM, SSH into it and switch into the project directory.

    vagrant up
    vagrant ssh
    cd /var/www/html

Install dependencies.

    composer install
    
### Create microservice

To quickly create a microservice 

    vagrant ssh
    cd /var/www/html
    app/console soap:generator:controller "https://url/to.wsdl" --controller-name="Default"
    
Now create a new User Api Key to access the REST Service:
    
    vim app/config/security.yml
    
    security:
      providers:
        in_memory:
          memory:
            users: 
              b79d5a38fe38ba79fcf3598e4f0bccdc:  { roles: [ 'ROLE_USER' ] }
    
After that you can access your newly created REST Service under: http://localhost:8080/app_dev.php/docs

## Links

- Local installation http://localhost:8080/app_dev.php/docs