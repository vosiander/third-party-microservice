#!/usr/bin/env bash
set -x #echo on

echo "###############################################################################"
echo "Installing Application"
sudo apt-get update -y
sudo apt-get install -y apache2 libapache2-mod-php5 php5 git openjdk-7-jre-headless php5-dev phpunit php5-curl
sudo apt-get install -y openjdk-7-jdk php5-xsl libxml2-utils imagemagick libmagickwand-dev php5-mcrypt vim git unzip
sudo apt-get install -y sshpass php5-gd php5-memcache php5-intl build-essential php-pear sendmail
sudo usermod -a -G www-data vagrant
sudo usermod -a -G dialout vagrant

echo "###############################################################################"
echo "Install php building tools"
sudo pear channel-discover pear.phpmd.org
sudo pear channel-discover pear.pdepend.org
sudo pear install --alldeps phpmd/PHP_PMD
wget https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar
chmod +x phpcs.phar
sudo mv phpcs.phar /usr/local/bin/phpcs
wget https://phar.phpunit.de/phploc.phar
chmod +x phploc.phar
sudo mv phploc.phar /usr/local/bin/phploc
wget http://static.pdepend.org/php/latest/pdepend.phar
chmod +x pdepend.phar
sudo mv pdepend.phar /usr/local/bin/pdepend
wget https://phar.phpunit.de/phpcpd.phar
chmod +x phpcpd.phar
sudo mv phpcpd.phar /usr/local/bin/phpcpd
wget http://phpdox.de/releases/phpdox.phar
chmod +x phpdox.phar
sudo mv phpdox.phar /usr/local/bin/phpdox
wget http://sourceforge.net/projects/jsch/files/jsch.jar/0.1.51/jsch-0.1.51.jar/download
sudo mv download /usr/share/ant/lib/jsch-0.1.51.jar
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
echo "memory_limit=4000M" | sudo tee -a /etc/php5/cli/php.ini

echo "###############################################################################"
echo "Activate setting changes for php"
echo 'Cleanup *.ini files to prevent "PHP Deprecated:  Comments starting with '\#' are deprecated in /...'
sudo find /etc/php5/cli/conf.d/ -name "*.ini" -exec sed -i -re 's/^(\s*)#(.*)/\1;\2/g' {} \;
echo "upload_max_filesize = 512M" | sudo tee -a /etc/php5/apache2/php.ini
echo "post_max_size = 100M" | sudo tee -a /etc/php5/apache2/php.ini
echo "memory_limit = 1024M" | sudo tee -a /etc/php5/apache2/php.ini
sudo /etc/init.d/apache2 restart
echo "xdebug.remote_enable=1
xdebug.profiler_enable=1
xdebug.remote_connect_back=1" | sudo tee -a /etc/php5/mods-available/xdebug.ini

echo "###############################################################################"
echo "Install Apache vhosts"
echo "ServerName localhost" >> /etc/apache2/apache2.conf
sudo a2enmod rewrite
sudo a2enmod proxy
sudo cat > /etc/apache2/sites-available/000-default.conf << EOF
<VirtualHost *:80>
        ServerAdmin webmaster@example.com
        DocumentRoot "/var/www/html/web"

        <Directory "/var/www/html/web">
                AllowOverride All
                Allow from All
        </Directory>
</VirtualHost>
<VirtualHost *:443>
        <Directory "/var/www/html/web">
                AllowOverride All
                Allow from All
        </Directory>

        SSLEngine On
        SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key
        SSLCertificateFile /etc/ssl/certs/ssl-cert-snakeoil.pem
        DocumentRoot "/var/www/html/web"
</VirtualHost>

EOF
sudo make-ssl-cert generate-default-snakeoil --force-overwrite
sudo a2enmod ssl
a2enmod rewrite
sudo service apache2 restart

echo "###############################################################################"
echo "Install app"
cd /vagrant
sudo rm -rf /var/www/html
sudo ln -s /vagrant /var/www/html
sudo rm -rf /var/www/html/app/cache/*
cd /var/www/html
composer install
cd /vagrant

echo "###############################################################################"
echo "Install done"